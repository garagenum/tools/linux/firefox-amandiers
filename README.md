# Un Firefox sécurisé sous Linux

**Objectifs :** 
- Configurer Firefox Quantum 60.1.1esr de sorte qu'il n'enregistre aucun identifiant et efface l'historique à la fin de chaque session, mais permette tout de même pour des raisons pratiques à l'utilisateur d'accéder à son historique de session en cours (débutant).
- Créer un profil sécurisé transférable dans n'importe quel Firefox de même version sous Linux (avancé). 
 
La distribution utilisée pour rédiger ce tuto est une Debian Stretch, mais le principe devrait être le même pour n'importe quelle distribution.

## Configuration sécurisée de Firefox pour un ordinateur public

### 1. Se rendre dans les préférences de Firefox :

![](screencaptures/Mozilla_Firefox_014.png) 

### 2. Se rendre dans l'onglet "Vie privée et sécurité" :

![](screencaptures/Preferences_Mozilla_Firefox_019.png) 

### 3. Dans la rubrique "Formulaires et mots de passe" : 

- Décocher "Enregistrer les identifiants et les mots de passe pour les sites web".
- Cliquer sur le menu "Identifiants enregistrés" en haut à droite ;
- Cliquer sur "Tout supprimer" :

![](screencaptures/Menu_012.png) 

**Attention !** Ces deux étapes sont capitales pour la confidentialité des usagers car par défaut, Firefox stocke en clair les identifiants enregistrés ainsi que leurs les mots de passe, comme on pourrait s'en apercevoir en cliquant sur le bouton "Afficher les mots de passe".

### 4. Dans la rubrique "Historique" :

- Choisir l'option "Utiliser les paramètres personnalisés pour l'historique" ; 
- Décocher "Toujours utiliser le mode de navigation privée" (car sinon l'historique ne serait pas conservé même en cours de session) ;
- Cocher "Conserver l’historique de navigation et des téléchargements" ;
- Cocher "Conserver l’historique des recherches et des formulaires" ; e
- Cocher "Vider l’historique lors de la fermeture de Firefox".
	
**Attention !** Il est très important de cocher cette dernière option, c'est elle qui permet d'effacer l'historique en fin de session.

## Créer un profil sécurisé, en faire le profil par défaut et le transférer sur un autre ordinateur

Il est possible de gérer les profils dans le navigateur, en tapant `about:profiles` dans la barre d'adresse. Ci dessous la méthode en ligne de commande et via les fichiers de configuration.

### 1. Créer un profil personnalisé

- Éventuellement, supprimer le profil par défaut déjà existant.
- Créer un profil via le gestionnaire de profils de Firefox accessible en ligne de commande (Firefox étant fermé au préalable) :

```
$ firefox -P
```

- Configurer le profil nouvellement créé comme ci-dessus (ne pas oublier d'y ajouter l'anti-pub Ublock-Origin).

### 2. Identifier les fichiers de configuration nécessaires

- Les fichiers de configuration du profil se situent dans le dossier `$HOME/.mozilla/firefox/`. Ils se trouvent dans un dossier dont le nom se termine par le nom du profil nouvellement créé, précédé d'une série de chiffres et de lettres. Dans notre exemple notre dossier se nomme `gxo3u9fa.amandiers` :

```
$ ls $HOME/.mozilla/firefox  
gxo3u9fa.amandiers
installs.ini     
profiles.ini
'Crash Reports'    
'Pending Pings'   
```

- Dans le même dossier, le fichier de configuration des profils se nomme `profiles.ini`. Il contient au minimum le profil par défaut (dont le nom se termine par `.default` s'il a été créé par Firefox et qui est associé au dossier éponyme). Exemple de fichier profile.ini :

```
$ cat $HOME/.mozilla/firefox/profiles.ini
[General]
StartWithLastProfile=1

[Profile0]
Name=default
IsRelative=1
Path=wi5mmusn.default
Default=1
```

- Si plusieurs profils coexistent, ils sont inscrits à la suite les uns des autres. Par exemple, si un profil "amandiers" côtoie le profil "default", il sera inscrit à la suite de ce dernier (noter l'absence de la variable `Default` qui définit le profil par défaut:

```
[Profile1]
Name=amandiers
IsRelative=1
Path=gxo3u9fa.amandiers
```

### 3. Faire du profil sécurisé le profil par défaut

- Dans le cas où il existe deux profils ou plus : il suffit de  rajouter la variable `Default=1` au profil que l'on souhaite utiliser par défaut et de la supprimer de celui qui l'était précédemment.
- Dans le cas où le profil est le seul existant, il est déjà celui par défaut. Sinon il faut supprimer le profil pré-existant (dans notre exemple `wi5mmusn.default`) du fichier profiles.ini (et éventuellement supprimer son dossier associé) et le remplacer par celui qu'on souhaite programmer (`gxo3u9fa.amandiers`) en oubliant pas d'y ajouter la variable `Default`. En cas de profil unique, notre dossier `profiles.ini` contiendra les lignes :

```
$ cat $HOME/.mozilla/firefox/profiles.ini
[General]
StartWithLastProfile=1

[Profile0]
Name=amandiers
IsRelative=1
Path=gxo3u9fa.amandiers
Default=1
```
On peut mettre la variable `StartWithLastProfile` à 0 pour faire en sorte que le profil par défaut soit toujours celui chargé à l'ouverture de Firefox, dans le cas où il existe plusieurs profils.

### 4. Transférer le profil sur une autre machine

Rien de plus simple : il s'agit de transférer le dossier du profil ainsi créé et de paramétrer le fichier `profiles.ini` correspondant dans l'arborecence des fichiers de l'utilisateur chez qui on souhaite effectuer ce transfert. Dans notre exemple, transférer le fichier et le dossier fournis avec ce tuto (après avoir décompressé le dossier au bon endroit) fera du profil "amandiers" le profil unique et par défaut de Firefox.