#!/bin/bash
#Installer un Firefox sécurisé sur un ordinateur public
#Assurez-vous d'avoir les droits requis pour effectuer ces opérations
#Script à lancer depuis le clone du dépôt : https://gitlab.com/garagenum/firefox-amandiers.git

echo "Pour quel utilisateur faut-il créer le profil ?"
read USER #Lit la réponse et l'inscrit dans une variable

CHECKUSER=$(grep $USER /etc/passwd | cut -d ':' -f1 2> /dev/null) #Récupère de le nom l'utilisateur demandé dans la liste des utilisateurs inscrits
CONFIGFOLDER=/home/$USER/.mozilla/firefox

installProfile(){
    tar xvf gxo3u9fa.amandiers.tar.gz -C $CONFIGFOLDER #Décompresse le dossier du profil "amandiers"
    cp $CONFIGFOLDER/profiles.ini $CONFIGFOLDER/profiles.ini.bak #Sauvegarde le fichier profiles.ini initialement présent
    cp profiles.ini $CONFIGFOLDER/profiles.ini #Remplace le fichier profiles.ini initialement présent par celui du profil "amandiers"
    echo "Les paramètres prendront effet au prochain lancement de Firefox !"
}

if [[ $USER == $CHECKUSER ]] #Vérifie que l'utilisateur demandé existe
then
    installProfile #Exécute la fonction installProfile
else
    "Cet utilisateur n'apparaît pas dans la liste des utilisateurs (/etc/passwd)"
fi